import React, { useEffect, useState } from 'react';
import dynamic from 'next/dynamic'
import formatPrice from '../utils/formatPrice';

// Hook
function useWindowSize() {
  // Initialize state with undefined width/height so server and client renders match
  // Learn more here: https://joshwcomeau.com/react/the-perils-of-rehydration/
  const [windowSize, setWindowSize] = useState({
    width: 300,
    height: 300,
  });
  useEffect(() => {
    // Handler to call on window resize
    function handleResize() {
      // Set window width/height to state
      setWindowSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    }
    // Add event listener
    window.addEventListener("resize", handleResize);
    // Call handler right away so state gets updated with initial window size
    handleResize();
    // Remove event listener on cleanup
    return () => window.removeEventListener("resize", handleResize);
  }, []); // Empty array ensures that effect is only run on mount
  return windowSize;
}

export default function App({ currency, historical }) {
  const size = useWindowSize()
  const [stocks, setStock] = useState(historical)

  useEffect(() => {
    // Início da conexão
    const connection = new WebSocket('wss://ws.bitstamp.net')

    // Ao abrir conexão (handshake concluído), sobescrever no tópico live_trades_currency
    connection.onopen = () =>
      connection.send(`{
        "event": "bts:subscribe",
        "data": {
            "channel": "live_trades_${ currency }"
        }
      }`)

    // Ao receber mensagem do tópico inscrito, atualizar dados na tela
    connection.onmessage = (message) => {
      if (!message?.data) return

      const { data = {} } = JSON.parse(message.data)
      const { timestamp, price: close } = data
      if (!timestamp || !close) return

      const date = new Date(timestamp * 1000).toISOString()
      setStock((currentStock) => currentStock.concat({ date, close }))
    }
  }, [ ])

  const onShareCurrency = () => {
    if (navigator.share) {
      navigator
        .share({
          title: `Price of ${ currency?.toUpperCase() }: ${ formatPrice(stocks[ stocks.length -1 ].close) }`,
          text: `🎉 Check out the price of ${ currency?.toUpperCase() } on this cool website 📈`,
          url: document.location.href,
        })
        .then(() => {
          console.log('Successfully shared');
        })
        .catch(error => {
          console.error('Something went wrong sharing', error);
        });
    }
  }

  const LLCurrencyPrice = dynamic(() => import('../components/currencyprice'))
  const LLBackground = dynamic(() => import('../components/background'))
  const LLHead = dynamic(() => import('next/head'))

  return (
    <>
      <LLHead>
        <title>Pwa Trab 2</title>
      </LLHead>
      <div className="app">
        <LLBackground width={size.width} height={size.height} />
        <div className="center">
          <LLCurrencyPrice
            currency={currency}
            stocks={stocks.filter(Boolean)}
            width={size.width}
            height={size.height}
          />
          <svg
            onClick={onShareCurrency}
            width="30px"
            height="30px"
            fill="none"
            stroke="#27273f"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg"
            style={{ cursor: 'pointer' }}
          >
            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M8.684 13.342C8.886 12.938 9 12.482 9 12c0-.482-.114-.938-.316-1.342m0 2.684a3 3 0 110-2.684m0 2.684l6.632 3.316m-6.632-6l6.632-3.316m0 0a3 3 0 105.367-2.684 3 3 0 00-5.367 2.684zm0 9.316a3 3 0 105.368 2.684 3 3 0 00-5.368-2.684z"></path>
          </svg>
        </div>
        <style jsx>{`
          .app,
          .center {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            overflow: hidden;
            display: flex;
            font-family: arial;
            flex-direction: column;
          }
          .disclaimer {
            margin-top: 35px;
            font-size: 11px;
            color: white;
            opacity: 0.4;
          }
          .center {
            align-items: center;
            justify-content: center;
          }
          .share-icon {
            position: absolute;
            right: 0;
            top: 10px;
            height: 18px;
            width: 18px;
            display: flex;
          }
          .share-icon:hover {
            cursor: pointer
          }
        `}</style>
      </div>
    </>
  )
}

/**
 * @typedef OHLCV
 * @param {string} timestamp
 * @param {number} open
 * @param {number} high
 * @param {number} low
 * @param {number} close
 * @param {number} volume
 */

const currencies = [ 'btcusd', 'btceur', 'btcgbp', 'btcpax', 'btcusdt', 'btcusdc', 'gbpusd', 'gbpeur', 'eurusd', 'ethusd', 'etheur', 'ethbtc', 'ethgbp', 'ethpax', 'ethusdt', 'ethusdc', 'xrpusd', 'xrpeur', 'xrpbtc', 'xrpgbp', 'xrppax', 'xrpusdt', 'uniusd', 'unieur', 'unibtc', 'ltcusd', 'ltceur', 'ltcbtc', 'ltcgbp', 'linkusd', 'linkeur', 'linkbtc', 'linkgbp', 'linketh', 'maticusd', 'maticeur', 'xlmusd', 'xlmeur', 'xlmbtc', 'xlmgbp', 'fttusd', 'ftteur', 'bchusd', 'bcheur', 'bchbtc', 'bchgbp', 'aaveusd', 'aaveeur', 'aavebtc', 'axsusd', 'axseur', 'algousd', 'algoeur', 'algobtc', 'compusd', 'compeur', 'compbtc', 'snxusd', 'snxeur', 'snxbtc', 'chzusd', 'chzeur', 'enjusd', 'enjeur', 'batusd', 'bateur', 'batbtc', 'mkrusd', 'mkreur', 'mkrbtc', 'zrxusd', 'zrxeur', 'zrxbtc', 'yfiusd', 'yfieur', 'yfibtc', 'sushiusd', 'sushieur', 'alphausd', 'alphaeur', 'grtusd', 'grteur', 'umausd', 'umaeur', 'umabtc', 'omgusd', 'omgeur', 'omgbtc', 'omggbp', 'kncusd', 'knceur', 'kncbtc', 'crvusd', 'crveur', 'crvbtc', 'sandusd', 'sandeur', 'audiousd', 'audioeur', 'audiobtc', 'storjusd', 'storjeur', 'usdtusd', 'usdteur', 'usdcusd', 'usdceur', 'usdcusdt', 'eurtusd', 'eurteur', 'daiusd', 'paxusd', 'paxeur', 'paxgbp', 'eth2eth', 'gusdusd' ]

export async function getStaticPaths() {
    const paths = currencies.map(currency => ({ params: { currency } }))

    return { paths, fallback: false }
}

export async function getStaticProps({ params: { currency } }) {
    // Fetches 30 days of currency
    const response = await fetch(`https://www.bitstamp.net/api/v2/ohlc/${ currency }/?step=3600&limit=1000`)

    /** @type {{ data: { ohlc: OHLCV[] }}} */
    const { data: { ohlc } = {} } = response.ok ? await response.json() : {}

    const historical = ohlc.map(
        ({ timestamp, close }) =>
            ({
                date: new Date(timestamp * 1000).toISOString(),
                close: Number(close)
            })
        )
    // Pass post data to the page via props
    return { props: { currency, historical } }
}