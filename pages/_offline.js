import React, { Component } from 'react';
import Head from 'next/head'

class Offline extends Component {
  render() {
    return (
      <>
        <Head>
          <title>Pwa Trab 2</title>
        </Head>
        <div>
            Sorry but you're offline
        </div>
    </>
    )
  }
}

export default Offline;
