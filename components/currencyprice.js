import Chart from './chart';
import formatPrice from '../utils/formatPrice';

export default function CurrencyPrice({ currency, stocks, width, height }) {
  const currentPrice = stocks[stocks.length - 1]?.close || 0;
  const firstPrice = stocks[0]?.close || 0;
  const diffPrice = currentPrice - firstPrice;
  const hasIncreased = diffPrice > 0;

  return (
    <div className="currency">
      <div className="title">
        <div>
          <strong>{ currency?.toUpperCase() }</strong> Price<br />
          <small>last 30 days</small>
        </div>
        <div className="spacer" />
        <div className="stats">
          <div className="current">
            {formatPrice(currentPrice)}
          </div>
          <div className={hasIncreased ? 'diffIncrease' : 'diffDecrease'}>
            {hasIncreased ? '+' : '-'}
            {formatPrice(diffPrice)}
          </div>
        </div>
      </div>
      <div className="chart">
        {
          stocks.length &&
          <Chart
            stocks={stocks}
            width={width * 0.6}
            height={height * 0.45}
            margin={{
              top: 0,
              left: 0,
              right: 0,
              bottom: 45
            }}
          />
        }
      </div>
      <style jsx>{`
        .currency {
          color: white;
          background-color: #27273f;
          border-radius: 6px;
          box-shadow: 0 2px 10px rgba(0, 0, 0, 0.7);
          display: flex;
          flex-direction: column;
        }
        .duration {
          font-weight: 100 !important;
          font-size: 14px;
          padding-bottom: 1px;
          border-bottom: 2px solid #6086d6;
        }
        .title,
        .stats {
          padding: 15px 15px 0;
          display: flex;
          flex-direction: row;
          align-items: center;
        }
        .title small {
          color: #6086d6;
        }
        .stats {
          padding: 0px;
          justify-content: flex-end;
          align-items: flex-end;
          flex-direction: column;
        }
        .current {
          font-size: 24px;
        }
        .diffIncrease,
        .diffDecrease {
          font-size: 12px;
          margin-left: .5rem;
        }
        .diffIncrease {
          color: #00f1a1;
        }
        .spacer {
          display: flex;
          flex: 1;
        }
        .chart {
          display: flex;
          flex: 1;
        }
      `}</style>
    </div>
  );
}
